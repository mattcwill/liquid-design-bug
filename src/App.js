import React from 'react';
import { Badge, Card, Theme } from '@liquid-design/liquid-design-react'

function App() {
  return (
    <Theme>
      <Card>
        <Badge>Hello</Badge>
      </Card>
    </Theme>
  );
}

export default App;
